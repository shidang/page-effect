init();
function init() {
    $(".circle").each(function(){
        $(this).css("border-color", "rgb" + getRGB());
        $(this).find(".circleCenter").css({
            "border-color": "rgb" + getRGB()
        });
    })
}
$("body").on("mousemove", function(e){
    var clientX = e.clientX;
    var clientY = e.clientY;
    $(this).find(".circleCenter").each(function(){
        var circle = $(this).parents(".circle").eq(0);
        circle.width = circle.width();
        circle.r = circle.width / 2;
        circle.border = (circle.outerWidth(true) - circle.width) / 2;
        var centerX = circle.offset().left + circle.border + circle.r;
        var centerY = circle.offset().top + circle.border + circle.r;
        var distance = Math.sqrt(Math.pow(centerX - clientX, 2) + Math.pow(centerY - clientY, 2));
        var dottedR = parseFloat($(this).css("border-width")) || 5;
        if(distance <= circle.r - dottedR) {
            $(this).css({
                "left": clientX - circle.offset().left - dottedR + "px",
                "top": clientY - circle.offset().top - dottedR + "px"
            })
        }
        var dotted = resemble(centerX, centerY, clientX, clientY, circle.r - dottedR);
        if(distance > circle.r - dottedR) {
            $(this).css({
                "left" :dotted.x + circle.r + "px",
                "top" :dotted.y + circle.r + "px"
            })
        }
    })

})

function resemble(centerX, centerY, mouseX, mouseY, distance){
    var d = Math.sqrt(Math.pow(centerX - mouseX, 2) + Math.pow(centerY - mouseY, 2));
    var dotted = {};
    dotted.x = (mouseX - centerX) * distance / d; 
    dotted.y = (mouseY - centerY) * distance / d;
    return dotted;
}

// 设置border的颜色
function setBorderColor(circle){
    $(circle).css("border-size", getRGB());
}

// 获取一个RGB
function getRGB(){
    var rgb = "(" + getRandomNumber() + "," + getRandomNumber() + "," + getRandomNumber() + ")";
    return rgb;
}

// 随机获取0-255中一个整数
function getRandomNumber(){
    var num = Math.random() * 255;
    num = Math.ceil(num);
    return num;
}