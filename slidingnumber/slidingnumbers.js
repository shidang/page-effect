'use strict';

/*
 * 触摸区域滑块滑动调整数值
 * Created by Sean on 2018/3/26.
 * defaults 		{object}
 * params[elem] 	{string} 滑动区
 * params[btn]  	{string} 滑动按钮
 * params[min]  	{number} 数值最小值
 * params[max]		{number} 数值最大值
 * params[step] 	{number} 均数
 * params[textEle] 	{string} 数值显示
 */
var sildingNumber = function sildingNumber(defaults) {
	this.sildingMian = document.getElementById(defaults.elem);
	this.sildingBtn = document.getElementById(defaults.btn);
	if (!this.sildingMian) {
		console.error('未找到元素容器');
		return;
	}
	if (!this.sildingBtn) {
		console.error('未找到滑动按钮');
		return;
	}
	this.min = defaults.min || 0;
	this.max = defaults.max || 10000;
	this.step = defaults.step || 10;
	this.text = document.getElementById(defaults.textEle) || null;
	this.action();
};
sildingNumber.prototype = {
	_disX: 0,
	_disY: 0,
	_maxHandlePos: sildingMian.clientWidth - sildingBtn.offsetWidth,
	action: function action() {
		var _this = this;
		sildingBtn.addEventListener('touchstart', function (e) {
			e.preventDefault();
			e.stopPropagation();
			_this.start(e);
		});
		sildingBtn.addEventListener('touchmove', function (e) {
			e.preventDefault();
			e.stopPropagation();
			_this.move(e);
		});
		sildingBtn.addEventListener('touchup', function (e) {
			e.preventDefault();
			e.stopPropagation();
			// 清除绑定移动事件
			sildingBtn.ontouchmove = null;
		});
	},
	start: function start(e) {
		var _this = this,
		    touch = e.touches[0],
		    x = touch.clientX,
		    y = touch.clientY,
		    disX = x - sildingBtn.offsetLeft,
		    disY = y - sildingBtn.offsetTop;
		_this._disX = disX;
		_this._disY = disY;
	},
	move: function move(e) {
		var _this = this,
		    touch = e.touches[0],
		    endX = touch.clientX,
		    endY = touch.clientY,
		    posX = endX - _this._disX,
		    posY = endY - _this._disY;
		Math.abs(posY) > Math.abs(posX) ? window.event.returnValue = true : ''; //避免禁用了下拉事件
		posX = (posX <= 0 ? 0 : posX) && (posX >= _this._maxHandlePos ? _this._maxHandlePos : posX);
		sildingBtn.style.left = posX + 'px';
		_this.calc(posX);
	},
	calc: function calc(posX) {
		var _this = this,
		    percentage = posX / (_this._maxHandlePos || 1),
		    actualint = _this.max - _this.min,
		    //实际区间
		value = _this.step * Math.round(percentage * actualint / _this.step) + _this.min;
		value = (value > _this.max ? _this.max : value) && (value < _this.min ? _this.min : value);
		_this.text ? _this.text.innerText = value : console.log(value);
	}
};

var silider = new sildingNumber({
	elem: 'sildingMian',
	btn: 'sildingBtn',
	textEle: 'accNum',
	step: 100,
	min: 1000,
	max: 20000
});