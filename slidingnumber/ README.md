    <!-- 数字滑动结构 -->
    html结构
    
      <div class="sildingmain">
        <h3 class="title" id="accNum">10000</h3>
        <div class="sildingNum" id="sildingMian">
          <span id="sildingBtn">|||</span>
        </div>
      </div>
      
      css样式
      .sildingmain {
          width: 90%;
          margin: 3rem auto;
        }
        
        .sildingmain .title {
          text-align: center;
          line-height: 3rem;
          font-size: 1rem;
        }
        
        .sildingmain .sildingNum {
          position: relative;
          width: 100%;
          height: 1rem;
          border-radius: 2rem;
          background: #ccc;
        }
        
        .sildingmain .sildingNum span {
          position: absolute;
          right: 0;
          top: -0.5rem;
          width: 2rem;
          height: 2rem;
          text-align: center;
          line-height: 2rem;
          font-size: 0.8rem;
          color: #fff;
          background: #a28989;
          border-radius: 50%;
        }
        
        
    引用js
    
    var silider = new sildingNumber({
	elem: 'sildingMian',
	btn: 'sildingBtn',
	textEle: 'accNum',
	step: 100,
	min: 1000,
	max: 20000
});
