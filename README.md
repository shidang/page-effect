# 常用效果合集
## [js原生效果实现](https://gitlab.com/shidang/page-effect/tree/master/JS%E5%BA%93)

使用原生js语法书写常见效果，如轮播，滚动，canvas，svg等！

## [jq+css3效果合集](https://gitlab.com/shidang/page-effect/tree/master/JQ+CSS3%E5%BA%93)

额，这是多个很炫很炫的css3效果实现。

## [ShootingStar](https://gitlab.com/shidang/page-effect/tree/master/ShootingStar)

用webpack+es6+canvas实现酷酷的流星雨动画

## [parallax 视觉差层次](https://gitlab.com/shidang/page-effect/tree/master/parallax%20%E8%A7%86%E8%A7%89%E5%B7%AE%E5%B1%82%E6%AC%A1demo)

使用parallax.js实现控件视觉差效果

## [canvas下雪效果](https://gitlab.com/shidang/page-effect/tree/master/canvas%20snow)

canvas画出下雪效果

## [明信片效果](https://gitlab.com/shidang/page-effect/tree/master/saveImage+%E6%96%87%E5%AD%97)

canvas+随机实现文字明信片图片效果，另存为图片效果



# 后面陆续增加